# Pose-guided action recognition

Author: [Jianren Wang](http://jrwangme.wixsite.com/website)

- Pretrained weights of C3D model w. and w.o. posed guide can be downloaded [here](https://drive.google.com/drive/folders/1f_JcpyF7u3HFvp8zOsTbV7rpaOtJ2td8?usp=sharing).
  